@extends('layouts.public')

@section('content')


<div class="row" style=" margin-top: 70px;">

        <!-- Blog Entries Column -->
        <div class="col-md-8">

          <h1 class="my-4">Horoscope 
            <small>2018</small>
          </h1>

          <!-- Blog Post -->
          @foreach($zodiac->posts as $post)
          <div class="card mb-4">
			<div class="card-body">
				 
				<div class="row">
					
					<div class="col-md-2">
         				<a href="/zodiacs/{{$zodiac->id}}/posts"><img style="max-height: 150px; max-width:100px;" class="card-img-top" src="{{'/storage/public/photos/'.$zodiac->picture}}"  alt="{{$zodiac->name}}"></a></div>
            			<div class="col-md-10">
             
              				<p class="card-text">{!! \Illuminate\Support\Str::words($post->description, 5,'....')  !!}
						
              					<a href="#" class="btn btn-primary btn-sm">Read More &rarr;</a></p>
              				</div>
            
				</div>	
              				
            		
            	<div class="card-footer text-muted">
              		Posted on {{$post->date}}, by 
              		<a href="#">{{$post->user->name}}</a>
           		</div>
        	</div>
        
        </div>
        @endforeach
         
          <!-- Pagination -->
          <ul class="pagination justify-content-center mb-4">
            <li class="page-item">
              <a class="page-link" href="#">&larr; Older</a>
            </li>
            <li class="page-item disabled">
              <a class="page-link" href="#">Newer &rarr;</a>
            </li>
          </ul>

        </div>
		
        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">

          <!-- Search Widget -->
          <div class="card my-4">
            <h5 class="card-header">Find your zodiac sign</h5>
            <div class="card-body">
            <div class="form-group">
             	<label class="label" for="date">Date of birht:</label>
              		<input type="text" name="date" id="datepick" placeholder="MM/DD/YY"/>
              		<p id="demo"></p>
            </div>
                <span class="input-group-btn">
                  <button class="btn btn-secondary" type="button" onclick="FindSign()">Go!</button>
                </span>
              </div>
            </div>
         

         

          <!-- Categories Widget -->
          <div class="card my-4">
            <h5 class="card-header">Categories</h5>
            <div class="card-body">
              <div class="row">
                <div class="col-lg-6">
                  <ul class="list-unstyled mb-0">
                    <li>
                      <a href="#">Web Design</a>
                    </li>
                    <li>
                      <a href="#">HTML</a>
                    </li>
                    <li>
                      <a href="#">Freebies</a>
                    </li>
                  </ul>
                </div>
                <div class="col-lg-6">
                  <ul class="list-unstyled mb-0">
                    <li>
                      <a href="#">JavaScript</a>
                    </li>
                    <li>
                      <a href="#">CSS</a>
                    </li>
                    <li>
                      <a href="#">Tutorials</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>

          <!-- Side Widget -->
          <div class="card my-4">
            <h5 class="card-header">Side Widget</h5>
            <div class="card-body">
              You can put anything you want inside of these side widgets. They are easy to use, and feature the new Bootstrap 4 card containers!
            </div>
          </div>

        </div>

      </div>
      <!-- /.row -->



@endsection