@extends('layouts.public')

@section('content')

    <div class="row">
      <!-- Jumbotron Header -->
      <header class="jumbotron my-4">
        <h1 class="display-3">What do the stars portend?</h1>
        <p class="lead">Horoscope 2018 give a nice overview or preview of the major influences on each zodiac sign over the course of the year and cover general trends, love, career, money, and family. Areas of expansion, innovation, restriction, and change are explored. </p>

          <p class="lead">Click on your Star Sign to be taken to your unique Horoscope page! Each Star Sign has a Daily Horoscope that is updated every morning, while new Weekly, Career, and Love Horoscopes will be posted throughout the week!</p>

        <p class="lead">To be one of the first to receive your Star Sign's Horoscopes, as well as all of the additional content provided on Astrology Answers, be sure to sign up for our e-mail list on our main page! </p>
        <a href="/find_zoodiac" class="btn btn-primary btn-lg">Find your zodiac sign!</a>
      </header>

      <!-- Page Features -->
      <div class="row text-center">
        @foreach($zodiacs as $zodiac)
        <div class="col-lg-3 col-md-6 mb-4">
          <div class="card">
            <a href="/zodiacs/{{$zodiac->id}}/posts"><img class="card-img-top" src="{{'/storage/public/photos/'.$zodiac->picture}}"  alt="{{$zodiac->name}}"></a>
            <div class="card-body">
              <h4 class="card-title">{{$zodiac->name}}</h4>
              <p class="card-text">Post Card text </p>
            </div>
            <div class="card-footer">
              <a href="/zodiacs/{{$zodiac->id}}/posts" class="btn btn-primary">View More!</a>
            </div>
          </div>

        </div>
        @endforeach
      </div>
      <!-- /.row -->
    </div>

      @endsection