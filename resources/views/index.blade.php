@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
    	<div class="col-md-8 ">
            <div class="panel panel-default">
	            <div class="panel-heading">Zodiac signs</div>
	            	<div  class="panel-body">

					    @if($zodiacs->count())
								
							@foreach($zodiacs as $zodiac)
							<div class="col-md-4">
								{{$zodiac->name }}
								<br/>
								{{ $zodiac->from . " ". $zodiac->to}}
								<br/>
							
								<a href="/zodiacs/{{$zodiac->id}}"><img style="max-width: 100px; max-height: 100px" src="{{'/storage/public/photos/'.$zodiac->picture}}" /></a>
								

							</div>
							@endforeach
					    @else
								
							<h4>Click here to add new zodiac sign. </h4>

					</div>
				</div>
			</div>
	</div>
</div>


    	@endif
@endsection