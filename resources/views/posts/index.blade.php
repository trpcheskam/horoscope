@extends('layouts.app')

@section('content')

    	<div class="col-md-8 ">
            <div class="panel panel-default">
	            <div class="panel-heading">Posts 

		            <form method="GET" action="">
		            		<div class="row">
			            		<div class="col-md-4">
				            	<select name="zodiac_id" class=" form-control ">
				            		<option class="form-control" name="zodiac_id" value="Filter by zodiac" selected="selected">Filter by zodiac..</option>
					            		@foreach($zodiacs as $zodiac)
					            			<option class="form-control" name="zodiac_id" value="{{$zodiac->id}}">{{$zodiac->name}}</option>
					            		@endforeach
				            	</select>
			            		</div>
								<div class="col-md-4">
								<select name="type_id" class="form-control">
				            		<option class="form-control" name="type_id" value="Filter by type" selected="selected">Filter by type..</option>
					            		@foreach($types as $type)
					            			<option class="form-control" name="type_id" value="{{$type->id}}">{{$type->name}}</option>
					            		@endforeach
				            	</select>
			            		</div>

			            		<div class="col-md-4">
			            			<button type="submit" class="btn btn-secondary btn-sm">Search</button>
			            		</div>
							</div>

		            </form>
		        </div> 
	   
	            	<div  class="panel-body">

					    @if($posts->count())
							<table class="table">
								<thead>
								<tr>
									<td class="heading">Date</td>
									<td>Zodiac</td>
									<td>Type</td>
									<td>Action</<td>
								</tr>
								</thead>
							@foreach($posts as $post)
								<tbody>
								<tr>	
									<td>{{$post->date}}</td>
							
									<td>{{$post->zodiac->name}}</td>

									<td>
										{{$post->posttype->name}}
									</td>

						   			<td>
										<a href="posts/edit/{{$post->id}}"><button type="button" class="btn btn-link">Edit</button></a>  

										<form action="posts/{{$post->id}}" method="POST">
											   {{ method_field('DELETE') }}
											    {{ csrf_field() }}
											    <button  type="submit" class="btn btn-link">Delete</button>
										</form>
									</td>
								</tr>
								</tbody>
							@endforeach

							</table>
					    @else
								
							<h4>Click <a href="/posts/create">here</a> to add new post. </h4>
						@endif
							{{ $posts->links()}}
					</div>
				</div>
			</div>


    	
@endsection