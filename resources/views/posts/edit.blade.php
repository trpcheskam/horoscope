@extends('layouts.app')

@section('content')

		<div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Post</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="/posts" enctype="multipart/form-data">
                        <input name="id" type="hidden" value="{{$post->id}}">

                        <input name="_method" type="hidden" value="PUT">
                        {{ csrf_field() }}
					
                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title" class="col-md-4 control-label">Title:</label>

                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control" name="title" value="{{ old('title') ? old('title') : $post->title}}"  autofocus>

                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-4 control-label">Description:</label>

                            <div class="col-md-6">
                                <textarea id="description" type="text" class="form-control" name="description" value="{{ old('description') ? old('description') : $post->description}}">{{ old('description') ? old('description') : $post->description}} </textarea>

                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                         <div class="form-group{{ $errors->has('zodiac_id') ? ' has-error' : '' }}">
                            <label for="zodiac_id" class="col-md-4 control-label">Zodiac:</label>

                            <div class="col-md-6">
                                <select id="zodiac_id"  class="form-control" name="zodiac_id">

                                    @foreach($zodiacs as $zodiac)
                                    <option name="zodiac_id" value="{{$zodiac->id}}">{{$zodiac->name}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('zodiac_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('zodiac_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                         <div class="form-group{{ $errors->has('post_type_id') ? ' has-error' : '' }}">
                            <label for="post_type_id" class="col-md-4 control-label">Post Type:</label>
                            
                            <div class="col-md-6">
                                 @foreach ($types as $type)
                                 <div class="radio-inline"><label><input type="radio" name="post_type_id" value="{{$type->id}}"  @if($post->post_type_id == $type->id) 
                                    checked="checked"
                                     @endif required>{{$type->name}}</label></div>
                              
                            @endforeach
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>



@endsection
