@extends('layouts.app')

@section('content')
		<div class="col-md-8 ">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Zodiac</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="/zodiacs" enctype="multipart/form-data">
                        <input name="id" type="hidden" value="{{$zodiac->id}}">

                        <input name="_method" type="hidden" value="PUT">
                        {{ csrf_field() }}
					
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name:</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') ? old('name') : $zodiac->name}}"  autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('from') ? ' has-error' : '' }}">
                            <label for="from" class="col-md-4 control-label">From:</label>

                            <div class="col-md-6">
                                <input id="from" type="text" class="form-control" name="from" value="{{ old('from') ? old('from') : $zodiac->from}}" >

                                @if ($errors->has('from'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('from') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('to') ? ' has-error' : '' }}">
                            <label for="to" class="col-md-4 control-label">To:</label>

                            <div class="col-md-6">
                                <input id="to" type="text" class="form-control" name="to"  value="{{ old('to') ? old('to') : $zodiac->to}}" >

                                @if ($errors->has('to'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('to') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

						<div class="form-group{{ $errors->has('picture') ? ' has-error' : '' }}">
							 <label for="to" class="col-md-4 control-label">Picture:</label>
                            <div class="col-md-6">
                                <input id="picture" type="file" class="form-control" name="picture" value="{{ old('name') ? old('name') : $zodiac->name}}">

                                @if ($errors->has('picture'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('picture') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>



@endsection