@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{$zodiac->name}}</div>

                <div class="panel-body">
                    <img style="max-width: 100px; max-height: 100px" src="{{'/storage/public/photos/'.$zodiac->picture}}" />
                </div>
            </div>
        </div>

	</div>
</div>

@endsection