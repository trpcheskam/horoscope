<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zodiac extends Model
{
    protected $fillable = ['name', 'from', 'to', 'picture'];

    public function posts()
    {
    	return $this->hasMany(Post::class);
    }
}
