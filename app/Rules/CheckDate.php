<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CheckDate implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
      $from       = date('Y-m-d', strtotime("2018-".$request->from));
      $to         = date('Y-m-d', strtotime("2018-".$request->to));

      if ($from > $to) {
        return false;
      }

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The validation error message.';
    }
}
