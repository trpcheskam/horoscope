<?php

namespace App\Http\Controllers\Blog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Zodiac;
use App\Post;
use App\PostType;

class HomeController extends Controller
{
    public function index() { 

    	$zodiacs= Zodiac::all();
    	$posts = Post::all();

    	return view('blog.home.index', compact('zodiacs', 'posts')); 

    }

    public function show($id)
    {

    	$zodiac = Zodiac::find($id);
    	$posts = Post::select('*')
  					->where('zodiac_id', $id)
  					->orderBy('id')
  					->paginate(5);

    	return view('blog.home.zodiac', compact('zodiac', 'posts'));
    }
}
