<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Http\Requests\PostsRequest;
use App\Zodiac;
use Carbon\Carbon;
use DB;
use App\PostType;
class PostsController extends Controller
{
	 public function __construct() {
    $this->middleware('auth');
  }


  public function index(Request $request)
  {
  	
  	
  	$types = PostType::all();
  	$zodiacs = Zodiac::all();

  	if(is_numeric($request->zodiac_id) && is_numeric($request->type_id))
  	{
  		$posts = Post::select('*')
  			->where('zodiac_id', $request->zodiac_id)
  			->where('post_type_id', $request->type_id)
  			->orderBy('id')
  			->paginate(5);
  	}
  	else if(is_numeric($request->zodiac_id))
  	{
  		$posts = Post::select('*')->where('zodiac_id', $request->zodiac_id)->orderBy('id')
  		->paginate(5);

  	
  	}
  	else if(is_numeric($request->type_id))
  	{
  		
  		$posts = Post::select('*')->where('post_type_id', $request->type_id)->orderBy('id')
  		->paginate(5);
  	}

  	else 
  	{
  		$posts = Post::paginate(5);
  	}
  	return view('posts.index', compact('posts', 'types', 'zodiacs'));
  }

  public function create()
  {
  	$zodiacs = Zodiac::all();
  	$types = PostType::all();

  	return view('posts.create', compact('zodiacs', 'types'));
  }

  public function store(PostsRequest $request)
  {
  	$post = new Post();
  	$post->title  = $request->title;
  	$post->description = $request->description;
  	$post->zodiac_id = $request->zodiac_id;
  	$post->date = Carbon::now()->format('Y-m-d');
  	$post->user_id = \Auth::id();
  	$post->post_type_id = $request->post_type_id;

  	$post->save();
  	$input=$request->all();
    $images=array();
            

    if($files=$request->file('images')){

        foreach($files as $file){
		    $fileNameWithExt = $file->getClientOriginalName();

		    $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
		    $extension = $file->getClientOriginalExtension();
		    $fileNameToStore = $fileName.'_'.time().'.'.$extension;

    		$path = $file->storeAs('public/photos/', $fileNameToStore, 'public');
		  
		    DB::table('post_images')->insert(["post_id" => $post->id, "image" => $fileNameToStore]);
  
        }
    }
     
  	

  	return redirect('/posts');
  }


  public function edit($id)
  {
		$post = Post::find($id);
		$zodiacs = Zodiac::all();
		$types = PostType::all();

		return view('posts.edit', compact('post', 'zodiacs', 'types'));
  }


  public function update(PostsRequest $request)
  {
  	$post = Post::find($request->id); // findOrFail.

  	$post->title  = $request->title;
  	$post->description = $request->description;
  	$post->zodiac_id = $request->zodiac_id;
  	$post->date = Carbon::now()->format('Y-m-d');
  	$post->user_id = \Auth::id();
  	$post->post_type_id = $request->post_type_id;

  	$post->save();

  		return redirect('/posts');
  }


  public function destroy($id)
  {
  	$post = Post::find($id);
  	
  	$post->delete();

  	return redirect('/posts');

  }


}