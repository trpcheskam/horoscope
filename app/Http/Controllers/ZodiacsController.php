<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ZodiacRequest;
use App\Zodiac;


class ZodiacsController extends Controller
{

  public function __construct() {
    $this->middleware('auth');
  }

   public function index()
   {
   	 $zodiacs = Zodiac::all();

   	 return view('index', compact('zodiacs'));
   }


   public function create()
   {

   	return view('zodiacs.create');
   }


   public function store(ZodiacRequest $request)

   {

    	$fileNameWithExt = $request->file('picture')->getClientOriginalName();


    	$fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);

    	$extension = request()->file('picture')->getClientOriginalExtension();

    	$fileNameToStore = $fileName.'_'.time().'.'.$extension;

    	$path = request()->file('picture')->storeAs('public/photos/', $fileNameToStore, 'public');

    	$zodiac = new Zodiac;

      $zodiac->name = $request->name;
      $zodiac->from = $request->from;
    	$zodiac->to = $request->to;
    	$zodiac->picture = $fileNameToStore;

     

    	$zodiac->save();

    	 return redirect('/zodiacs')->with('message', 'Success');

   }



   private function _check_dates($request) {


      $from       = date('Y-m-d', strtotime("2018-".$request->from));
      $to         = date('Y-m-d', strtotime("2018-".$request->to));

      if ($from > $to) {
        return false;
      }

   }

   public function edit($id)
   {

    $zodiac = Zodiac::find($id);
     return view('zodiacs.edit', compact('zodiac'));
   }

   public function update(ZodiacRequest $request)
   {
    $zodiac = Zodiac::find($request->id);

    $zodiac->name = $request->name;
    $zodiac->from = $request->from;
    $zodiac->to = $request->to;

    if($request->file('picture')){
    $fileNameWithExt = $request->file('picture')->getClientOriginalName();
    $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
    $extension = request()->file('picture')->getClientOriginalExtension();
    $fileNameToStore = $fileName.'_'.time().'_updated'.'.'.$extension;
    $path = request()->file('picture')->storeAs('public/photos/', $fileNameToStore, 'public');
    $zodiac->picture = $fileNameToStore;
    }

    else 
    $zodiac->picture = $zodiac->picture;
    $zodiac->save();

    return redirect('/zodiacs'); 

   }


   public function destroy(Request $request)
   {
      $zodiac = Zodiac::find($request->id);

      $zodiac->delete();


      return redirect('/zodiacs');
   }

   public function show(Request $request)
   {
    $zodiac = Zodiac::find($request->id);

    return view('zodiacs.show', compact('zodiac'));
   }
}
