<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['id', 'title', 'description', 'zodiac_id', 'date', 'user_id'];

    public function zodiac()
    {
    	return $this->belongsTo(Zodiac::class);
    }


    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    //relacija za posttype

    public function posttype()
    {
    	return $this->belongsTo(PostType::class, "post_type_id");
    }
}
