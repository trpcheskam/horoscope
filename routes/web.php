<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/zodiacs', 'ZodiacsController@index');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/zodiacs/create', 'ZodiacsController@create');

Route::post('/zodiacs', 'ZodiacsController@store');

Route::get('/zodiacs/edit/{id}', 'ZodiacsController@edit');

Route::put('/zodiacs', 'ZodiacsController@update');

Route::delete('/zodiacs/{id}', 'ZodiacsController@destroy');

Route::get('/zodiacs/{id}', 'ZodiacsController@show');


Route::get('/posts', 'PostsController@index');
Route::get('/posts/create', 'PostsController@create');
Route::post('/posts', 'PostsController@store');
Route::get('/posts/edit/{id}', 'PostsController@edit');
Route::put('/posts', 'PostsController@update');

Route::delete('/posts/{id}', 'PostsController@destroy');

Route::get('/', 'Blog\HomeController@index');

Route::get('/zodiacs/{id}/posts', 'Blog\HomeController@show');
