<?php

use Illuminate\Database\Seeder;
use App\PostType;

class PostTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('truncate table post_types;');

        DB::table('post_types')
        	->insert(["name" => "daily"]);

		DB::table('post_types')
        	->insert(["name" => "weekly"]);

        DB::table('post_types')
        	->insert(["name" => "monthly"]);        	

        //PostType::create(["daily"]);
		//PostType::create(["weekly"]);
		//PostType::create(["monthly"]);
    }
}
